package com.changyou.skynet.dao.impl;

import java.util.Date;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import spring.SpringTxTestCase;

import com.changyou.skynet.dao.IncomeDaoI;
import com.changyou.skynet.model.TIncome;

public class IncomeDaoImplTest extends SpringTxTestCase {
	@Autowired
	IncomeDaoI incomeDao;
	
	@Test
	public void TestOne(){
		TIncome o = new TIncome();
		o.setDesc("test");
		o.setMoney("100");
		o.setDate(new Date());
		incomeDao.saveOrUpdate(o);
		TIncome o2 = incomeDao.get("from TIncome t");
		System.out.println(o2.getDesc());
	}
	
	@Test
	public void TestTwo() {
		TIncome o = incomeDao.get("from TIncome t");
		System.out.println(o.getDesc());
	}

}
