<!DOCTYPE HTML>
<html lang="utf-8">
<head>
<meta charset="utf-8">
<title>jQuery File Upload Demo - Basic version</title>
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
<!-- Generic page styles -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQuery-File-Upload-9.5.0/css/style.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jQuery-File-Upload-9.5.0/css/jquery.fileupload.css">
</head>
<body>
	<div class="container">
		<span class="btn btn-success fileinput-button"> 
		<span>上传一个文件...</span> 
			<input id="fileupload" type="file" name="files[]" multiple>
		</span> 
		<div id="progress" class="progress">
			<div class="progress-bar progress-bar-success"></div>
		</div>
		<div id="files" class="files"></div>
		<br>
	</div>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/jQuery-File-Upload-9.5.0/js/vendor/jquery.ui.widget.js"></script>
	<script src="${pageContext.request.contextPath}/resources/jQuery-File-Upload-9.5.0/js/jquery.iframe-transport.js"></script>
	<script src="${pageContext.request.contextPath}/resources/jQuery-File-Upload-9.5.0/js/jquery.fileupload.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script>
		/*jslint unparam: true */
		/*global window, $ */
		$(function() {
			'use strict';
			// Change this to the location of your server-side upload handler:
			var url = 'formUpload';
			$('#fileupload').fileupload(
					{
						url : url,
						dataType : 'json',
						done : function(e, data) {
							$.each(data.result.files, function(index, file) {
								$('<p/>').text(file.name).appendTo('#files');
							});
						},
						progressall : function(e, data) {
							var progress = parseInt(data.loaded / data.total
									* 100, 10);
							$('#progress .progress-bar').css('width',
									progress + '%');
						}
					}).prop('disabled', !$.support.fileInput).parent()
					.addClass($.support.fileInput ? undefined : 'disabled');
		});
	</script>
</body>
</html>
