<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<
<script type="text/javascript">
	var loginDialog;
	var loginTabs;
	var defaultUserInfoDialog;
	$(function() {
		loginDialog = $('#loginDialog').show().dialog({
			modal : true,
			closable : false,
			buttons : [ {
				text : '注册',
				handler : function() {
					$('#registerDialog').dialog('open');
				}
			}, {
				text : '登录',
				handler : function() {
					loginFun();
				}
			} ]
		});

		defaultUserInfoDialog = $('#defaultUserInfoDialog').show().dialog({
			top : 0,
			left : 200
		});
	});
	
	// 登录
	function loginFun() {
		if (layout_west_tree) {
			loginTabs = $('#loginTabs').tabs('getSelected');
			var form = loginTabs.find('form');
			console.info(form);
			
			if (form.form('validate')) {
				parent.$.messager.progress({
					title : '提示',
					text : '数据处理中，请稍后...'
				});
				$.post('${pageContext.request.contextPath}/userController/login', form.serialize(), function(result) {
					if (result.success) {
						if (!layout_west_tree_url) {
	                        layout_west_tree.tree({
	                        	url : '${pageContext.request.contextPath}/resourceController/tree',
	                        	onBeforeLoad : function(node, param) {
	                        		parent.$.messager.progress({
	                        			title : '提示',
	                        			text : '数据处理中, 请稍后...'
	                        		});
	                        	}
	                        });
	                    }
						loginDialog.dialog('close');
					} else {
						$.messager.alert('错误', result.msg, 'error');
					}
					parent.$.messager.progress('close');
				},"JSON");
			}
		}
	}
</script>

<div id="loginDialog" title="用户登录"
	style="width: 330px; height: 220px; overflow: hidden; display: none;">
	<div id="loginTabs" class="easyui-tabs"
		data-options="fit:true,border:false">
		<div title="用户输入模式" style="overflow: hidden; padding: 10px;">
			<form method="post">
				<table class="table table-hover table-condensed">
					<tr>
						<th>登录名</th>
						<td><input name="name" type="text" placeholder="请输入登录名"
							class="easyui-validatebox" data-options="required:true"
							value="孙宇"></td>
					</tr>
					<tr>
						<th>密码</th>
						<td><input name="pwd" type="password" placeholder="请输入密码"
							class="easyui-validatebox" data-options="required:true"
							value="123456"></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>

<div id="defaultUserInfoDialog" title="系统测试账号"
	style="width: 300px; height: 260px; overflow: hidden; display: none;">
	<div class="well well-small" style="margin: 3px;">请大家不要随意更改系统默认账户的信息，如果想测试，请自己新建立用户进行测试</div>
	<div class="well well-small" style="margin: 3px;">
		<div>
			<span class="badge">1</span>超管：孙宇/123456
		</div>
		<div>
			<span class="badge badge-success">2</span>资源管理员：admin1/123456
		</div>
		<div>
			<span class="badge badge-warning">3</span>角色管理员：admin2/123456
		</div>
		<div>
			<span class="badge badge-important">4</span>用户管理员：admin3/123456
		</div>
		<div>
			<span class="badge badge-info">5</span>数据源管理员：admin4/123456
		</div>
		<div>
			<span class="badge badge-inverse">6</span>BUG管理员：admin5/123456
		</div>
		<div>
			<span class="badge">7</span>来宾用户：guest/123456
		</div>
	</div>
</div>