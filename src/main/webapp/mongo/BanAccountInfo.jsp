<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>封停数据查询</title>
<jsp:include page="../inc.jsp"></jsp:include>
</head>
<body class="easyui-layout">
	<div data-options="region:'center'">
		<table id="dg" title="封停数据查询结果" class="easyui-datagrid"
			toolbar="#toolbar" rownumbers="true" fitColumns="true"
			url="BanAccountInfoFindAll" pagination="true" singleSelect="true"
			iconCls="icon-search" data-options="fit:true">
			<thead>
				<tr>
					<th field="account" width="50">account</th>
					<th field="charName" width="50">charName</th>
					<th field="ip" width="50">ip</th>
					<th field="ruleName" width="50">ruleName</th>
					<th field="date" width="50">date</th>
				</tr>
			</thead>
		</table>
		<div id="toolbar" style="padding: 5px; height: auto">
			<form id="searchTableForm" method="post">
				account: <input name="account" style="width: 80px">
				charName: <input name="charName" style="width: 80px"> ip: <input
					name="ip" style="width: 80px">
				<hr>
				ruleName: <input name="ruleName" style="width: 80px"> date:
				<input name="date" class="easyui-datebox" style="width: 120px">
				<a href="#" class="easyui-linkbutton" onclick="searchFun()"
					iconCls="icon-search">查询</a> <a href="#" class="easyui-linkbutton"
					onclick="cleanFun()" iconCls="icon-reload">重设条件</a>
			</form>
		</div>
	</div>
	<script>
		$.serializeObject = function(form) {
			var o = {};
			$.each(form.serializeArray(), function(index) {
				if (o[this['name']]) {
					o[this['name']] = o[this['name']] + "," + this['value'];
				} else {
					o[this['name']] = this['value'];
				}
			});
			return o;
		};
		// 查询函数
		function searchFun() {
			var formData = $.serializeObject($('#searchTableForm'));
			console.info(formData);
			$('#dg').datagrid('load', formData);
		}
		// 清空查询条件
		function cleanFun() {
			$('#searchTableForm input').val('');
			dataGrid.datagrid('load', {});
		}
	</script>
</body>
</html>