<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="../inc.jsp"></jsp:include>
<title>任务管理</title>
<script type="text/javascript">
	var taskDatagrid = null;
	var url = null;
	$(function() {
		taskDatagrid = $('#dg')
				.datagrid(
						{
							url : '/SkyNet/TaskController/datagrid',
							fit : true,
							fitColumns : true,
							border : false,
							pagination : true,
							idField : 'id',
							pageSize : 10,
							pageList : [ 10, 20, 30, 40, 50 ],
							sortName : 'createdatetime',
							sortOrder : 'desc',
							checkOnSelect : false,
							selectOnCheck : false,
							nowrap : false,
							striped : true,
							rownumbers : true,
							singleSelect : true,
							columns : [ [
									{
										field : 'id',
										title : '编号',
										hidden : true
									},
									{
										field : 'taskId',
										title : '任务ID',
										width : 100
									},
									{
										field : 'taskDescribe',
										title : '任务描述',
										width : 100
									},
									{
										field : 'jobId',
										title : '作业标识',
										width : 100
									},
									{
										field : 'cronExpression',
										title : '执行条件',
										width : 100
									},
									{
										field : 'isEffect',
										title : '是否生效',
										width : 100
									},
									{
										field : 'isStart',
										title : '是否开启',
										width : 100
									},
									{
										field : 'createDate',
										title : '创建日期',
										width : 100
									},
									{
										field : 'updateDate',
										title : '更改日期',
										width : 100
									},
									{
										field : 'updateName',
										title : '更改人',
										width : 100
									},
									{
										field : 'operate',
										title : '操作',
										width : 100,
										formatter : function(value, row, index) {
											console.info(index);
											return '<a href="#" class="operateButton" onclick="funStartOrStop('
													+ index
													+ ',1)" >执行</a>'
													+ '<a href="#" class="operateButton" onclick="funStartOrStop('
													+ index + ',0)" >停止</a>';
										}
									} ] ],
							toolbar : '#tb',
							onLoadSuccess : function(data) {
								$(".operateButton").linkbutton();
							}

						});
	});
	// 删除一个任务
	function funDelete(id) {
		if (id == undefined) {
			var rows = taskDatagrid.datagrid('getSelections');
			if (rows.length == 0) {
				console.info('没有选择任务');
				return;
			}
			id = rows[0].id;
			console.info('删除任务id为:' + id);
		}
		parent.$.messager.confirm('询问', '您是否要删除当前任务', function(result) {
			if (result) {
				parent.$.messager.progress({
					title : '提示',
					text : '数据处理中，请稍后....'
				});
				console.info('开始删除任务');
				$.post('/SkyNet/TaskController/delete', {
					id : id
				}, function(result) {
					if (result.success) {
						parent.$.messager.alert('提示', result.msg, 'info');
						$('#dg').datagrid('reload');
					}
					parent.$.messager.progress('close');
				}, 'JSON');
			}
		});
	}
	// 添加一个任务
	function funAddTask() {
		console.info("add task");
		$('#dlg').dialog('open').dialog('setTitle', '添加任务');
		$('#fm').form('clear');
		$('#isEffect').combobox('select', 1);
		url = '/SkyNet/TaskController/addTask';
	}
	// 修改一个任务
	function funEdit() {
        var row = taskDatagrid.datagrid('getSelected');
        if (row){
            $('#dlg').dialog('open').dialog('setTitle','修改任务');
            $('#fm').form('load',row);
            url = '/SkyNet/TaskController/editTask?id=' + row.id;
        }
	}
	// 保存任务
	function saveTask() {
		$('#fm').form('submit', {
			url : url,
			onSubmit : function() {
				return $(this).form('validate');
			},
			success : function(result) {
				var result = eval('(' + result + ')');
				console.info(result);
				if (false == result.success) {
					$.messager.show({
						title : 'Error',
						msg : '添加不成功!'
					});
				} else {
					$('#dlg').dialog('close');
					$('#dg').datagrid('reload');
				}
			}
		});
	}
	// 执行或停止一个任务
	function funStartOrStop(i, isStart) {
		var rows = taskDatagrid.datagrid('getRows');
		var taskId = rows[i];
		console.info(i + ':' + taskId.id + ':' + isStart);
		$.getJSON("/SkyNet/TaskController/startOrStopTask ", {
			id : taskId.id,
			Start : isStart
		}, function(data) {
			console.info(data);
			if (data.success == true) {
				$.messager.show({
					title : '执行执行结果',
					msg : '任务操作成功',
					timeout : 2000,
					showType : 'slide'
				});
				$('#dg').datagrid('reload');
			}
		}, "json");
	}
</script>
</head>
<body class="easyui-layout" data-options="fit:true">

	<div data-options="region:'center'">
		<table id="dg"></table>
	</div>

	<div id="tb" style="padding: 5px; height: auto">
		<div style="margin-bottom: 5px">
			<a href="#" onclick="funAddTask()" class="easyui-linkbutton"
				iconCls="icon-add" plain="true">添加</a> 
			<a href="#" onclick="funEdit()" class="easyui-linkbutton"
                iconCls="icon-edit" plain="true">修改</a>
			<a href="#" onclick="funDelete()" class="easyui-linkbutton"
				iconCls="icon-remove" plain="true">删除</a>
			
		</div>
	</div>

	<div id="dlg" class="easyui-dialog"
		style="width: 500px; height: 400px; padding: 10px 20px" closed="true"
		buttons="#dlg-buttons">
		<form id="fm" method="post" accept-charset="UTF-8"
			onsubmit="document.charset='UTF-8'" novalidate>
			<div class="fitem">
				<label>任务标识:</label> <input name="taskId" class="easyui-validatebox"
					required="true">
			</div>
			<div class="fitem">
				<label>任务描述:</label> <input name="taskDescribe">
			</div>
			<div class="fitem">
				<label>作业标识:</label> <select name="JobId" class="easyui-combobox"
					data-options="width:140,height:29,editable:false,panelHeight:'auto'">
					<c:forEach items="${taskTypeList}" var="taskType">
						<option value="${taskType}">${taskType}</option>
					</c:forEach>
				</select>
			</div>
			<div class="fitem">
				<label>执行条件:</label> <input name="cronExpression"
					class="easyui-validatebox" required="true">
			</div>
			<div class="fitem">
				<label>是否生效:</label> <select id="isEffect" name="isEffect"
					class="easyui-combobox"
					data-options="editable:false,panelHeight:'auto'">
					<option value="0">否</option>
					<option value="1" selected='selected'>是</option>
				</select>

			</div>
		</form>
	</div>
	<div id="dlg-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton"
			iconCls="icon-ok" onclick="saveTask()">Save</a> <a
			href="javascript:void(0)" class="easyui-linkbutton"
			iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Cancel</a>
	</div>

	<style type="text/css">
#fm {
	margin: 0;
	padding: 10px 30px;
}

.ftitle {
	font-size: 14px;
	font-weight: bold;
	padding: 5px 0;
	margin-bottom: 10px;
	border-bottom: 1px solid #ccc;
}

.fitem {
	margin-bottom: 5px;
}

.fitem label {
	display: inline-block;
	width: 80px;
}
</style>
</body>
</html>