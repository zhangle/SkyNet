<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<head>
<title>Home</title>
<link rel="stylesheet" type="text/css"
	href="/SkyNet/resources/easyUI/themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css"
	href="/SkyNet/resources/easyUI/themes/icon.css">
<script type="text/javascript"
	src="/SkyNet/resources/js/jquery-1.8.3.js"></script>
<script type="text/javascript"
	src="/SkyNet/resources/easyUI/jquery.easyui.min.js"></script>
</head>
<body class="easyui-layout">
	<div data-options="region:'north', border:false" style="height: 100px;">
		<img alt="畅游Mongo" src="/SkyNet/resources/image/header.png"
			style="height: 100px; width: 260px;">
	</div>
	<div data-options="region:'south', border:false" style="height: 5px;"></div>
	<div data-options="region:'west',title:'West111',split:true"
		style="width: 200px;">
		<div title="Tree Menu">
			<ul id="tt1" class="easyui-tree">
				<li><a href="#" onclick="addTab('BanAccountInfo','/SkyNet/BanAccountInfo')">table</a>
				</li>
			</ul>
		</div>
	</div>
	<div data-options="region:'center',title:'center title'"
		style="padding: 2px; background: #eee;">
		<div id="tt" class="easyui-tabs" data-options="fit:true" style="">
		</div>
	</div>
	<script>
		function addTab(title, url) {
			if ($('#tt').tabs('exists', title)) {
				$('#tt').tabs('select', title);
			} else {
				var content = '<iframe scrolling="yes" frameborder="0"  src="'
						+ url + '" style="width:100%;height:100%;"></iframe>';
				$('#tt').tabs('add', {
					title : title,
					content : content,
					closable : true
				});
			}
		}
	</script>
</body>
</html>
