<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Test</title>
<jsp:include page="../inc.jsp"></jsp:include>
<script type="text/javascript">
	var dataGrid;

	$(function() {
		dataGrid = $('#dg').datagrid({
			url : '/SkyNet/userController/test',
			pagination : true,
			singleSelect : true,
			columns : [ [ {
				field : 'name',
				title : 'Name',
				width : 100
			}, {
				field : 'pwd',
				title : 'password',
				width : 100
			} ] ]
		});
	});

	function searchFun() {
		console.info("searchFun");
		dataGrid.datagrid('load', $.serializeObject($('#searchForm')));
	}
</script>
</head>
<body>
	<table id="dg"></table>
	<form id="searchForm">
		<input name="name" /> <a href="javascript:void(0);"
			class="easyui-linkbutton"
			data-options="iconCls:'brick_add',plain:true" onclick="searchFun();">过滤条件</a>
		<a href="javascript:void(0);" class="easyui-linkbutton"
			data-options="iconCls:'brick_delete',plain:true"
			onclick="cleanFun();">清空条件</a>
	</form>
</body>
</html>