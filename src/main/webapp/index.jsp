<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>Happy</title>
<jsp:include page="inc.jsp"></jsp:include>
<script type="text/javascript">
	var index_layout;
	var index_tabs;
	var index_tabsMenu;
	$(function(){
		index_layout = $('#index_layout').layout({
			fit : true
		});
		index_tabs = $('#index_tabs').tabs({
			fit : true,
			border : false,
			onContextMenu : function(e, title) {
				console.info("on context menu");
				e.preventDefault();
				index_tabsMenu.menu('show',{
					left : e.pageX,
					top : e.pageY
				}).data('tabTitle', title);
			}
			
		});
		index_tabsMenu = $('#index_tabsMenu').menu({
			onClick : function(item) {
				var curTabTitle = $(this).data('tabTitle');
				var type = $(item.target).attr('title');
				console.info(type);
			}
		});
	});
</script>
</head>
<body>
	<div id="index_layout">
		<div data-options="region:'north',href:'${pageContext.request.contextPath}/layout/north.jsp'" style="height: 70px; overflow:hidden;" class="logo"></div>
		<div data-options="region:'west',href:'${pageContext.request.contextPath}/layout/west.jsp',split:true" title="导航" style="width:200px; overflow:hidden;"></div>
		<div data-options="region:'center'" title="欢迎使用happy" style="overflow:hidden;">
			<div id="index_tabs" style="overflow:hidden;">
				<div title="首页" data-options="border:false" style="overflow:hidden;">
				</div>
			</div>
		</div>
		<div data-options="region:'east'" title="日历" style="width: 230px; overflow: hidden;"></div>
		<div data-options="region:'south',border:false" style="height:30px; overflow:hidden;"></div>
	</div>
	
	<div id="index_tabsMenu" style="width:120px; display:none;">
		<div title="refresh" data-options="iconCls:'transmit'">refresh</div>
		<div class="menu-sep"></div>
		<div title="close" data-options="iconCls:'delete'">close</div>
	</div>
</body>
</html>