<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- bootstrap -->
<link href="${pageContext.request.contextPath}/resources/bootstrap-2.3.1/css/bootstrap.min.css" rel="stylesheet" media="screen">

<!-- 引入EasyUI -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/easyUI/themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/easyUI/themes/icon.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-1.8.3.js" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/extjQuery.js" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/easyUI/jquery.easyui.min.js" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/easyUI/locale/easyui-lang-zh_CN.js" charset="utf-8"></script>

<!-- 扩展EasyUI -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/extEasyUI.js?v=201305241044" charset="utf-8"></script>
<!-- 扩展EasyUI Icon -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/style/extEasyUIIcon.css?v=201305301906" type="text/css">



