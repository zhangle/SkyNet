package com.changyou.skynet.web.domain.page;

import java.util.ArrayList;
import java.util.List;

/**
 * EasyUI DataGrid模型
 * 
 * @author 孙宇
 * 
 */
public class DataGrid<E> implements java.io.Serializable {
    
    private Long total = 0L;
    private List<E> rows = new ArrayList<E>();
    
    public Long getTotal() {
        return total;
    }
    
    public void setTotal(Long total) {
        this.total = total;
    }
    
    public List<E> getRows() {
        return rows;
    }
    
    public void setRows(List<E> rows) {
        this.rows = rows;
    }
}
