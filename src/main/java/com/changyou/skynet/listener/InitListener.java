package com.changyou.skynet.listener;

import javax.servlet.ServletContextEvent;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.changyou.skynet.task.service.TaskService;

/**
 * 系统初始化监听器,在系统启动时运行,进行一些初始化工作
 * 
 * @author laien
 * 
 */
public class InitListener implements javax.servlet.ServletContextListener {

    public void contextDestroyed(ServletContextEvent arg0) {

    }

    public void contextInitialized(ServletContextEvent event) {
//        WebApplicationContext webApplicationContext = WebApplicationContextUtils
//                .getWebApplicationContext(event.getServletContext());
//        TaskService taskService = (TaskService) webApplicationContext
//                .getBean("taskService");
//        taskService.initTaskFromDB();
    }

}
