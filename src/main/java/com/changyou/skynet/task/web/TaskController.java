package com.changyou.skynet.task.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.changyou.skynet.task.domain.pageModel.TimeTask;
import com.changyou.skynet.task.service.TaskService;
import com.changyou.skynet.web.domain.page.DataGrid;
import com.changyou.skynet.web.domain.page.Json;

@Controller
@RequestMapping("/TaskController")
public class TaskController {

    @Autowired
    private TaskService taskService;
    
    private static final Logger logger = LoggerFactory
            .getLogger(TaskController.class);
    /**
     * 定时任务管理列表 页面跳转
     * 
     * @return
     */
    @RequestMapping("/taskList")
    public ModelAndView timeTask(HttpServletRequest request) {
        logger.info("taskList");
        request.setAttribute("taskTypeList", taskService.getTaskTypeList());
        return new ModelAndView("task/TaskList");
    }

    @ResponseBody
    @RequestMapping("/datagrid")
    public DataGrid<TimeTask> datagrid(TimeTask timeTask, HttpServletRequest request,
            HttpServletResponse response) {
        return taskService.datagrid(timeTask);
    }
    
    /**
     * 启动或者停止任务
     */
    @ResponseBody
    @RequestMapping("/startOrStopTask")
    public Json startOrStopTask(String id, String Start,HttpServletRequest request) {
        Json j = new Json();
        boolean isSuccess = taskService.startOrStopTask(id,Start);
        j.setSuccess(isSuccess);
        j.setMsg(isSuccess?"定时任务管理更新成功":"定时任务管理更新失败");
        return j;
    }
    
    /**
     * 添加一个任务
     */
    @ResponseBody
    @RequestMapping("addTask")
    public Json addTask(TimeTask task) {
        logger.info("add Task!");
        Json j = new Json();
        if (null != task) {
            logger.info(task.toString());
            try {
            boolean ret = taskService.addTask(task);
            j.setSuccess(ret);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
        return j;
    }
    /**
     * 修改一个任务
     */
    @ResponseBody
    @RequestMapping("editTask")
    public Json editTask(TimeTask task) {
        logger.info("edit Task!");
        Json j = new Json();
        if (null != task) {
            logger.info(task.toString());
            try {
            boolean ret = taskService.editTask(task);
            j.setSuccess(ret);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
        return j;
    }
    @RequestMapping("/delete")
    @ResponseBody
    public Json delete(String id) {
    	Json j = new Json();
    	logger.info("delete id:{}", id);
    	j.setSuccess(taskService.delete(id));
    	return j;
    }
    @RequestMapping("/init")
    @ResponseBody
    public Json initTasksFromDB() {
        Json j = new Json();
        logger.info("initTasksFromDB begin.");
        taskService.initTaskFromDB();
        j.setMsg("启动所有任务成功!");
        j.setSuccess(true);
        logger.info("initTasksFromDB end.");
        return j;
    }
}
