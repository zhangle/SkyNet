package com.changyou.skynet.task.service;

import com.changyou.skynet.task.domain.pageModel.TimeTask;
import com.changyou.skynet.web.domain.page.DataGrid;

public interface TaskService {
    public boolean addTask(TimeTask task);
    public DataGrid<TimeTask> datagrid(TimeTask task);
    public boolean startOrStopTask(String id, String start);
    public boolean delete(String id);
    public boolean initTaskFromDB();
    public Object getTaskTypeList();
    public boolean editTask(TimeTask task);
}
