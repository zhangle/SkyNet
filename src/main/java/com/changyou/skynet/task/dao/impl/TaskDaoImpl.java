package com.changyou.skynet.task.dao.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.changyou.skynet.task.dao.TaskDao;
import com.changyou.skynet.task.domain.TSTimeTaskEntity;
import com.changyou.skynet.task.domain.pageModel.TimeTask;
import com.changyou.skynet.util.SkyNetDaoUtil;

@Repository
public class TaskDaoImpl implements TaskDao {

    @Autowired
    private SkyNetDaoUtil skyNetDaoUtil;

    /**
     * 添加任务
     */
    public Serializable save(TSTimeTaskEntity entity) {
        return skyNetDaoUtil.save(entity);
    }

    /**
     * 查看任务
     */
    public List<TSTimeTaskEntity> find(String hql, Map<String, Object> params) {
        return skyNetDaoUtil.find(hql, params);
    }

    /**
     * 通过任务主键，查询一个任务
     * @param id
     * @return
     */
    public TSTimeTaskEntity find(String id) {
        return skyNetDaoUtil.get(TSTimeTaskEntity.class, id);
    }
    /**
     * 通过任务主键，删除一个任务
     */
    public void delete(String id) {
        skyNetDaoUtil.delete(this.find(id));
    }

    public long count(String hql, Map<String, Object> params) {
        return skyNetDaoUtil.count(hql, params);
    }

}
