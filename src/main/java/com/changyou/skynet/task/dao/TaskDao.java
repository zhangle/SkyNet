package com.changyou.skynet.task.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.changyou.skynet.task.domain.TSTimeTaskEntity;

public interface TaskDao {
    public Serializable save(TSTimeTaskEntity entity);
    public List<TSTimeTaskEntity> find(String hql, Map<String, Object> params);
    public TSTimeTaskEntity find(String id);
    public void delete(String id);
    public long count(String hql, Map<String, Object> params);
}
