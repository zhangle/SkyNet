package com.changyou.skynet.task.domain.pageModel;

public class TimeTask {
    /** id */
    private java.lang.String id;
    /** 任务ID */
    private java.lang.String taskId;
    /** 任务描述 */
    private java.lang.String taskDescribe;
    /** 作业ID */
    private java.lang.String JobId;
    /** cron表达式 */
    private java.lang.String cronExpression;
    /** 是否生效了0未生效,1生效了 */
    private java.lang.String isEffect;
    /** 是否运行0停止,1运行 */
    private java.lang.String isStart;
    /** 创建时间 */
    private java.util.Date createDate;
    /** 修改时间 */
    private java.util.Date updateDate;
    /** 修改人名称 */
    private java.lang.String updateName;

    public java.lang.String getIsStart() {
        return isStart;
    }

    public void setIsStart(java.lang.String isStart) {
        this.isStart = isStart;
    }

    public java.lang.String getTaskId() {
        return taskId;
    }

    public void setTaskId(java.lang.String taskId) {
        this.taskId = taskId;
    }

    public java.lang.String getIsEffect() {
        return isEffect;
    }

    public void setIsEffect(java.lang.String isEffect) {
        this.isEffect = isEffect;
    }

    public java.lang.String getId() {
        return id;
    }

    public void setId(java.lang.String id) {
        this.id = id;
    }

    public java.lang.String getTaskDescribe() {
        return taskDescribe;
    }

    public void setTaskDescribe(java.lang.String taskDescribe) {
        this.taskDescribe = taskDescribe;
    }

    public java.lang.String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(java.lang.String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public java.util.Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(java.util.Date createDate) {
        this.createDate = createDate;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
    }

    public java.lang.String getUpdateName() {
        return updateName;
    }

    public void setUpdateName(java.lang.String updateName) {
        this.updateName = updateName;
    }

    public java.lang.String getJobId() {
        return JobId;
    }

    public void setJobId(java.lang.String jobId) {
        JobId = jobId;
    }

    @Override
    public String toString() {
        return "TimeTask [id=" + id + ", taskId=" + taskId + ", taskDescribe="
                + taskDescribe + ", JobId=" + JobId + ", cronExpression="
                + cronExpression + ", isEffect=" + isEffect + ", isStart="
                + isStart + ", createDate=" + createDate + ", updateDate="
                + updateDate + ", updateName=" + updateName + "]";
    }
}
