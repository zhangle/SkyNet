package com.changyou.skynet.pageModel;

import java.util.List;

public class SessionInfo implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String name;
	private String ip;
	private List<String> resourceList;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public List<String> getResourceList() {
		return resourceList;
	}

	public void setResourceList(List<String> resourceList) {
		this.resourceList = resourceList;
	}

}
