package com.changyou.skynet.dao.impl;

import org.springframework.stereotype.Repository;

import com.changyou.skynet.dao.UserDaoI;
import com.changyou.skynet.model.Tuser;

@Repository
public class UserDaoImpl extends BaseDaoImpl<Tuser> implements UserDaoI {
}
