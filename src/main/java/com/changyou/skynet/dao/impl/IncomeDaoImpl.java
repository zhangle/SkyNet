package com.changyou.skynet.dao.impl;

import org.springframework.stereotype.Repository;

import com.changyou.skynet.dao.IncomeDaoI;
import com.changyou.skynet.model.TIncome;

@Repository
public class IncomeDaoImpl extends BaseDaoImpl<TIncome> implements IncomeDaoI {

}
