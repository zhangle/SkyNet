package com.changyou.skynet.dao.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.changyou.skynet.dao.ResourceTypeDaoI;
import com.changyou.skynet.model.Tresourcetype;

@Repository
public class ResourceTypeDaoImpl extends BaseDaoImpl<Tresourcetype> implements
		ResourceTypeDaoI {

	@Override
	@Cacheable(value = "resourceTypeDaoCache", key = "#id")
	public Tresourcetype getById(String id) {

		return super.get(Tresourcetype.class, id);
	}

}
