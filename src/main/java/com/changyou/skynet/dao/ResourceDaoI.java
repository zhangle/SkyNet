package com.changyou.skynet.dao;

import com.changyou.skynet.model.Tresource;


/**
 * 资源数据库操作类
 * 
 * @author 孙宇
 * 
 */
public interface ResourceDaoI extends BaseDaoI<Tresource> {

}
