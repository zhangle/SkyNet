package com.changyou.skynet.dao;

import com.changyou.skynet.model.Tresourcetype;

public interface ResourceTypeDaoI extends BaseDaoI<Tresourcetype> {
	public Tresourcetype getById(String id);
}
