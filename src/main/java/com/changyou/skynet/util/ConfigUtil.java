package com.changyou.skynet.util;

import java.util.ResourceBundle;

public class ConfigUtil {

	private static final ResourceBundle bundle = java.util.ResourceBundle.getBundle("config");
	
	public static final String getSessionInfoName() {
		return bundle.getString("sessionInfoName");
	}
	
	public static final String get(String key) {
		return bundle.getString(key);
	}
}
