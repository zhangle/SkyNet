package com.changyou.skynet.controller;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.changyou.skynet.pageModel.Resource;
import com.changyou.skynet.pageModel.SessionInfo;
import com.changyou.skynet.pageModel.Tree;
import com.changyou.skynet.service.ResourceServiceI;
import com.changyou.skynet.util.ConfigUtil;

@Controller
@RequestMapping("/resourceController")
public class ResourceController {
	private static final Logger logger = Logger
			.getLogger(ResourceController.class);

	@Autowired
	private ResourceServiceI resourceService;

	@RequestMapping("/tree")
	@ResponseBody
	public List<Tree> tree(HttpSession session) {
		logger.info("tree");
		SessionInfo sessionInfo = (SessionInfo) session.getAttribute(ConfigUtil
				.getSessionInfoName());
		return resourceService.tree(sessionInfo);
	}

	@RequestMapping("/test")
	public String test(HttpSession session) {
		logger.info("test");
		return "test";
	}

	@RequestMapping(value = "/formUpload", method = RequestMethod.POST)
	public String handleFormUpload(MultipartHttpServletRequest request) {
		Iterator<String> itr = request.getFileNames();
		MultipartFile mpf;
		List<Image> list = new LinkedList<Image>();
		while (itr.hasNext()) {
			mpf = request.getFile(itr.next());

			String newFilenameBase = UUID.randomUUID().toString();
			String originalFileExtension = mpf.getOriginalFilename().substring(
					mpf.getOriginalFilename().lastIndexOf("."));
			String newFilename = newFilenameBase + originalFileExtension;
			String storageDirectory = "e:/";
			String contentType = mpf.getContentType();

			File newFile = new File(storageDirectory + "/" + newFilename);
			try {
				mpf.transferTo(newFile);

//				BufferedImage thumbnail = Scalr.resize(ImageIO.read(newFile),
//						290);
//				String thumbnailFilename = newFilenameBase + "-thumbnail.png";
//				File thumbnailFile = new File(storageDirectory + "/"
//						+ thumbnailFilename);
//				ImageIO.write(thumbnail, "png", thumbnailFile);
//
//				Image image = new Image();
//				image.setName(mpf.getOriginalFilename());
//				image.setThumbnailFilename(thumbnailFilename);
//				image.setNewFilename(newFilename);
//				image.setContentType(contentType);
//				image.setSize(mpf.getSize());
//				image = imageDao.create(image);
//
//				image.setUrl("/picture/" + image.getId());
//				image.setThumbnailUrl("/thumbnail/" + image.getId());
//				image.setDeleteUrl("/delete/" + image.getId());
//				image.setDeleteType("DELETE");

//				list.add(image);

			} catch (IOException e) {
				
			}

		}
		return "test";
	}
	
	@RequestMapping("/manager")
	public String manager() {
		return "/admin/resource";
	}
	
	@RequestMapping("/treeGrid")
	@ResponseBody
	public List<Resource> treeGrid(HttpSession session) {
		SessionInfo sessionInfo = (SessionInfo) session.getAttribute(ConfigUtil.getSessionInfoName());
		return resourceService.treeGrid(sessionInfo);
	}
}
