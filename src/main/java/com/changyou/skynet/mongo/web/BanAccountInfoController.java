package com.changyou.skynet.mongo.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.changyou.skynet.mongo.domain.BanAccountInfo;
import com.changyou.skynet.mongo.domain.BanAccountInfoForm;
import com.changyou.skynet.mongo.domain.Page;
import com.changyou.skynet.mongo.service.BanAccountInfoService;
import com.changyou.skynet.web.domain.page.DataGrid;

@Controller
public class BanAccountInfoController {
    private static final Logger logger = LoggerFactory
            .getLogger(BanAccountInfoController.class);
    
    @Autowired
    private BanAccountInfoService service;
    
    /**
     * Simply selects the home view to render by returning its name.
     */
    @RequestMapping(value = "/BanAccountInfo")
    public String home() {
        String[] aStrings = { "A", "B", "C" };
        logger.info("BanAccountInfo. {},{},{}", aStrings);
        return "mongo/BanAccountInfo";
    }
    
    @RequestMapping(value = "/BanAccountInfoFindAll", method = RequestMethod.POST)
    @ResponseBody
    public DataGrid<BanAccountInfo> banAccountInfoFindAll(BanAccountInfoForm info, Page page) {
        DataGrid<BanAccountInfo> ret = service.find(info, page);
        return ret;
    }

}
