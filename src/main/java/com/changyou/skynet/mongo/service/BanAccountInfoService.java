package com.changyou.skynet.mongo.service;

import com.changyou.skynet.mongo.domain.BanAccountInfo;
import com.changyou.skynet.mongo.domain.BanAccountInfoForm;
import com.changyou.skynet.mongo.domain.Page;
import com.changyou.skynet.web.domain.page.DataGrid;

public interface BanAccountInfoService {
    public DataGrid<BanAccountInfo> find(BanAccountInfoForm info, Page page);
}
