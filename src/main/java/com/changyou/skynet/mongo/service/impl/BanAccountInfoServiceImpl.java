package com.changyou.skynet.mongo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.changyou.skynet.mongo.dao.BanAccountInfoDao;
import com.changyou.skynet.mongo.domain.BanAccountInfo;
import com.changyou.skynet.mongo.domain.BanAccountInfoForm;
import com.changyou.skynet.mongo.domain.Page;
import com.changyou.skynet.mongo.service.BanAccountInfoService;
import com.changyou.skynet.web.domain.page.DataGrid;

@Service
public class BanAccountInfoServiceImpl implements BanAccountInfoService {
    
    @Autowired
    private BanAccountInfoDao dao;
    
    public DataGrid<BanAccountInfo> find(BanAccountInfoForm info, Page page) {
        DataGrid<BanAccountInfo> ret = new DataGrid<BanAccountInfo>();
        Query q = new Query();
        Criteria criteria = null;
        if (null != info.getAccount() && !"".equals(info.getAccount())) {
            criteria = Criteria.where("account");
            criteria.is(info.getAccount());
            q.addCriteria(criteria);
        }
        if (null != info.getCharName() && !"".equals(info.getCharName())) {
            criteria = Criteria.where("charName");
            criteria.is(info.getCharName());
            q.addCriteria(criteria);
        }
        if (null != info.getIp() && !"".equals(info.getIp())) {
            criteria = Criteria.where("ip");
            criteria.is(info.getIp());
            q.addCriteria(criteria);
        }
        if (null != info.getRuleName() && !"".equals(info.getRuleName())) {
            criteria = Criteria.where("ruleName");
            criteria.is(info.getRuleName());
            q.addCriteria(criteria);
        }
        if (null != info.getDate() && !"".equals(info.getDate())) {
            criteria = Criteria.where("date");
            criteria.is(info.getDate());
            q.addCriteria(criteria);
        }
        // 设置分页
        q.skip((page.getPage() - 1) * page.getRows());
        q.limit(page.getRows());
        List<BanAccountInfo> rows = dao.getBanAccountInfo(q);
        ret.setRows(rows);
        
        //置设返回数量
        ret.setTotal(dao.getCount(q));
        return ret;
    }
}
