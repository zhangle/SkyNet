package com.changyou.skynet.mongo.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.changyou.skynet.mongo.domain.BanAccountInfo;

@Repository
public class BanAccountInfoDao {
    
    @Autowired
    private MongoTemplate mongoTemplate;
    
    public List<BanAccountInfo> getBanAccountInfo(Query query) {
        List<BanAccountInfo> retAccountInfos = mongoTemplate.find(query,
                BanAccountInfo.class);
        return retAccountInfos;
    }
    
    public long getCount(Query query) {
        return mongoTemplate.count(query, BanAccountInfo.class);
    }
}
