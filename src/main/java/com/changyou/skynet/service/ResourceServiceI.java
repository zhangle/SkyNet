package com.changyou.skynet.service;

import java.util.List;

import com.changyou.skynet.pageModel.Resource;
import com.changyou.skynet.pageModel.SessionInfo;
import com.changyou.skynet.pageModel.Tree;

public interface ResourceServiceI {
	/**
	 * 获得资源树(资源类型为菜单类型)
	 * 
	 * 通过用户ID判断，他能看到的资源
	 * 
	 * @param sessionInfo
	 * @return
	 */
	public List<Tree> tree(SessionInfo sessionInfo);

	public List<Resource> treeGrid(SessionInfo sessionInfo);
}
