package com.changyou.skynet.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.changyou.skynet.dao.ResourceDaoI;
import com.changyou.skynet.model.Tresource;
import com.changyou.skynet.pageModel.Resource;
import com.changyou.skynet.pageModel.SessionInfo;
import com.changyou.skynet.pageModel.Tree;
import com.changyou.skynet.service.ResourceServiceI;

@Service
public class ResourceServiceImpl implements ResourceServiceI {

	private static final Logger logger = Logger
			.getLogger(ResourceServiceImpl.class);
	@Autowired
	private ResourceDaoI resourceDao;

	public List<Tree> tree(SessionInfo sessionInfo) {
		List<Tresource> l = null;
		List<Tree> lt = new ArrayList<Tree>();

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("resourceTypeId", "0");

		if (sessionInfo != null) {
			params.put("userId", sessionInfo.getId());
			l = resourceDao
					.find("select distinct t from Tresource t join fetch t.tresourcetype type join fetch t.troles role join role.tusers user where type.id = :resourceTypeId and user.id = :userId order by t.seq",
							params);
		} else {
			l = resourceDao
					.find("select distinct t from Tresource t join fetch t.tresourcetype type where type.id = :resourceTypeId order by t.seq",
							params);
		}

		if (l != null && l.size() > 0) {
			for (Tresource r : l) {
				Tree tree = new Tree();
				BeanUtils.copyProperties(r, tree);
				if (r.getTresource() != null) {
					tree.setPid(r.getTresource().getId());
				}
				tree.setText(r.getName());
				tree.setIconCls(r.getIcon());
				Map<String, Object> attr = new HashMap<String, Object>();
				attr.put("url", r.getUrl());
				tree.setAttributes(attr);
				lt.add(tree);
			}
		}
		return lt;
	}

	@Override
	public List<Resource> treeGrid(SessionInfo sessionInfo) {
		List<Tresource> l = null;
		List<Resource> lr = new ArrayList<Resource>();

		Map<String, Object> params = new HashMap<String, Object>();
		if (sessionInfo != null) {
			params.put("userId", sessionInfo.getId());
			l = resourceDao
					.find("select distinct t from Tresource t join fetch t.tresourcetype type join fetch t.troles role join role.tusers user where user.id = :userId order by t.seq",
							params);
		} else {
			l = resourceDao
					.find("select distinct t from Tresource t join fetch t.tresourcetype type order by t.seq",
							params);
		}

		if (l != null && l.size() > 0) {
			for (Tresource t : l) {
				logger.debug(t.toString());
				Resource r = new Resource();
				BeanUtils.copyProperties(t, r);
				if (t.getTresource() != null) {
					r.setPid(t.getTresource().getId());
					r.setPname(t.getTresource().getName());
				}
				r.setTypeId(t.getTresourcetype().getId());
				r.setTypeName(t.getTresourcetype().getName());
				if (t.getIcon() != null && !t.getIcon().equalsIgnoreCase("")) {
					r.setIconCls(t.getIcon());
				}
				logger.debug(r.toString());
				lr.add(r);
			}
		}
		logger.debug(lr.toString());
		return lr;
	}

}
