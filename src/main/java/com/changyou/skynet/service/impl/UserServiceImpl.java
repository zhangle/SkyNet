package com.changyou.skynet.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.changyou.skynet.dao.UserDaoI;
import com.changyou.skynet.model.Tresource;
import com.changyou.skynet.model.Trole;
import com.changyou.skynet.model.Tuser;
import com.changyou.skynet.pageModel.User;
import com.changyou.skynet.service.UserServiceI;
import com.changyou.skynet.util.MD5Util;

@Service
public class UserServiceImpl implements UserServiceI {

    @Autowired
    private UserDaoI userDao;

    @Override
    public User login(User user) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", user.getName());
        params.put("pwd", MD5Util.md5(user.getPwd()));
        Tuser t = userDao.get(
                "from Tuser t where t.name =:name and t.pwd = :pwd", params);
        if (null != t) {
            BeanUtils.copyProperties(t, user);
            return user;
        }
        return null;
    }

    @Override
    public List<String> resourceList(String id) {
        List<String> resourceList = new ArrayList<String>();
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", id);
        Tuser t = userDao
                .get("from Tuser t join fetch t.troles role join fetch role.tresources resource where t.id = :id",
                        params);
        if (null != t) {
            Set<Trole> roles = t.getTroles();
            if (null != roles && !roles.isEmpty()) {
                for (Trole role : roles) {
                    Set<Tresource> resources = role.getTresources();
                    if (resources != null && !resources.isEmpty()) {
                        for (Tresource resource : resources) {
                            if (null != resource && null != resource.getUrl()) {
                                resourceList.add(resource.getUrl());
                            }
                        }
                    }
                }
            }
        }
        return resourceList;
    }

    @Override
    synchronized public void reg(User user) throws Exception {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", user.getName());
        if (userDao.count("select count(*) from Tuser t where t.name = :name", params) > 0) {
            throw new Exception("登录名已存在！");
        } else {
            Tuser u = new Tuser();
            u.setId(UUID.randomUUID().toString());
            u.setName(user.getName());
            u.setPwd(MD5Util.md5(user.getPwd()));
            u.setCreatedatetime(new Date());
            userDao.save(u);
        }
    }
}
