package com.changyou.skynet.service;

import java.util.List;

import com.changyou.skynet.pageModel.User;

public interface UserServiceI {
    public User login(User user);
    public List<String> resourceList(String id);
    void reg(User user) throws Exception;
}
